package org.ergea.foodapp.scheduler;

import org.ergea.foodapp.entity.Order;
import org.ergea.foodapp.repository.OrderRepository;
import org.ergea.foodapp.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class OrderScheduler {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderService orderService;

    @Scheduled(cron = "0 0 0 * * ?")
    public void processOrders() {
        List<Order> orders = orderRepository.findByIsCompleteFalseAndOrderTimeBefore(LocalDateTime.now().minusDays(1));
        orders.forEach(order -> {
            order.setIsComplete(true);
            orderRepository.save(order);
        });
    }
}

