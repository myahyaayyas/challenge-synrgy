package com.example.challange3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;

public class OrderSystemTest {

    private OrderSystem orderSystem;

    @BeforeEach
    void setUp() {
        orderSystem = new OrderSystem();
    }

    @Test
    void testPlaceOrder() {
        String simulatedInput = "1\n1\n1\n";
        provideSimulatedInput(simulatedInput);

        assertDoesNotThrow(() -> orderSystem.placeOrder());
    }

    @Test
    void testCancelOrder() {
        String simulatedInput = "1\n1\n1\ny\n";
        provideSimulatedInput(simulatedInput);

        assertDoesNotThrow(() -> orderSystem.cancelOrder());
    }

    @Test
    void testConfirmAndPay() {
        String simulatedInput = "y\n";
        provideSimulatedInput(simulatedInput);

        assertDoesNotThrow(() -> orderSystem.confirmAndPay());
    }

    private void provideSimulatedInput(String simulatedInput) {
        InputStream savedStandardInput = System.in;
        System.setIn(new ByteArrayInputStream(simulatedInput.getBytes()));

        assertDoesNotThrow(() -> System.setIn(savedStandardInput));
    }
}
