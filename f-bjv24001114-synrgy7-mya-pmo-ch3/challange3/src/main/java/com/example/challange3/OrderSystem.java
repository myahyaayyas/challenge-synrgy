package com.example.challange3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class OrderSystem {
    private List<MenuItem> menu;
    private Map<String, Order> orders;
    private Scanner scanner;

    public OrderSystem() {
        menu = new ArrayList<>();
        orders = new HashMap<>();
        scanner = new Scanner(System.in);
        initializeMenu();
    }

    public static void main(String[] args) throws IOException {
        OrderSystem orderSystem = new OrderSystem();
        orderSystem.runOrderSystem();
    }

    private void runOrderSystem() throws IOException {
        boolean isRunning = true;
        while (isRunning) {
            displayMenu();
            Optional<Integer> choice = getUserChoice();
            switch (choice.orElse(0)) {
                case 1:
                    placeOrder();
                    break;
                case 2:
                    displayOrders();
                    break;
                case 3:
                    cancelOrder();
                    break;
                case 4:
                    confirmAndPay();
                    break;
                case 5:
                    isRunning = false;
                    break;
                default:
                    System.err.println("Pilihan tidak valid. Silakan pilih kembali.");
                    break;
            }
        }
        scanner.close();
    }

    public Optional<Integer> getUserChoice() {
        while (!scanner.hasNextInt()) {
            System.out.println("Input tidak valid. Silakan masukkan nomor.");
            scanner.next();
        }
        return Optional.of(scanner.nextInt());
    }

    private void displayMenu() {
        System.out.println("\n===============================");
        System.out.println("Selamat Datang Di Restoran Kami");
        System.out.println("===============================");
        System.out.println("1. Pesan Makanan");
        System.out.println("2. Pesanan Anda");
        System.out.println("3. Hapus Pesanan");
        System.out.println("4. Konfirmasi dan Pembayaran");
        System.out.println("5. Keluar");
        System.out.print("Pilihan Anda: ");
    }

    private void initializeMenu() {
        menu.add(new MenuItem("Mie Goreng", 12000.0));
        menu.add(new MenuItem("Nasi Goreng", 15000.0));
        menu.add(new MenuItem("Ayam Goreng", 18000.0));
        menu.add(new MenuItem("Soto Ayam", 20000.0));
    }

    public void placeOrder() {
        displayFoodMenu();
        Optional<Integer> menuNumber = getUserInput("Masukkan nomor makanan yang ingin dipesan: ");
        menuNumber.ifPresent(number -> {
            if (number >= 1 && number <= menu.size()) {
                MenuItem selectedFood = menu.get(number - 1);
                Optional<Integer> quantity = getUserInput("Masukkan jumlah pesanan: ");
                quantity.ifPresent(qty -> {
                    if (qty > 0) {
                        updateOrder(selectedFood, qty);
                    } else {
                        System.err.println("Jumlah pesanan tidak valid.");
                    }
                });
            } else {
                System.err.println("Nomor makanan tidak valid, Silahkan pilih nomor pesanan 1-4");
            }
        });
    }

    private void displayFoodMenu() {
        System.out.println("\n======= Menu Makanan =======");
        menu.forEach(item -> System.out.println((menu.indexOf(item) + 1) + ". " + item.getName() + " - Rp " + item.getPrice()));
        System.out.println("============================");
    }

    private Optional<Integer> getUserInput(String message) {
        System.out.print(message);
        while (!scanner.hasNextInt()) {
            System.err.println("Input tidak valid. Silakan masukkan nomor.");
            scanner.next();
        }
        return Optional.of(scanner.nextInt());
    }

    public void displayOrders() {
        if (orders.isEmpty()) {
            System.out.println("Belum ada pesanan.");
        } else {
            System.out.println("\n======= Pesanan Saat Ini =======");
            orders.values().forEach(order -> {
                MenuItem item = order.getItem();
                int quantity = order.getQuantity();
                System.out.println(item.getName() + " - " + quantity + " porsi");
            });
            System.out.println("=================================");
        }
    }

    private void updateOrder(MenuItem selectedFood, int quantity) {
        String itemName = selectedFood.getName();
        if (orders.containsKey(itemName)) {
            Order existingOrder = orders.get(itemName);
            int currentQuantity = existingOrder.getQuantity();
            existingOrder.setQuantity(currentQuantity + quantity);
        } else {
            Order newOrder = new Order(selectedFood, quantity);
            orders.put(itemName, newOrder);
        }
        System.out.println("Pesanan " + itemName + " sebanyak " + quantity + " berhasil ditambahkan.");
    }

    public void confirmAndPay() throws IOException {
        double total = calculateTotal();
        displayOrderDetails(total);
        boolean isConfirmed = getConfirmation("Apakah Anda yakin ingin melakukan pembayaran?");
        if (isConfirmed) {
            saveReceipt(total);
            clearOrders();
        } else {
            System.out.println("Pembayaran dibatalkan.");
        }
    }

    private double calculateTotal() {
        return orders.values().stream()
                .mapToDouble(Order::getSubtotal)
                .sum();
    }

    private void displayOrderDetails(double total) {
        System.out.println("\n===== Konfirmasi dan Pembayaran =====");
        orders.values().forEach(order -> {
            MenuItem item = order.getItem();
            int quantity = order.getQuantity();
            double price = item.getPrice();
            double subtotal = order.getSubtotal();
            System.out.println(item.getName() + " - " + quantity + " x Rp " + price + " = Rp " + subtotal);
        });
        System.out.println("---------------------------------------+");
        System.out.println("Total Pembayaran: Rp " + total);
    }

    private boolean getConfirmation(String message) {
        Scanner userOption = new Scanner(System.in);
        System.out.println("\n" + message + "(y/n)");
        String pilihanuser = userOption.next();

        while (!pilihanuser.equalsIgnoreCase("y") && !pilihanuser.equalsIgnoreCase("n")) {
            System.err.println("pilahn anda bukan y/n silahkan pilih y/n");
            System.out.println("\n" + message + "(y/n)");
            pilihanuser = userOption.next();
        }

        return pilihanuser.equalsIgnoreCase("y");
    }

    private void saveReceipt(double total) throws IOException {
        try {
            FileWriter writer = new FileWriter("struk_pembayaran.txt");
            writer.write("===== Struk Pembayaran =====\n");
            orders.values().forEach(order -> {
                MenuItem item = order.getItem();
                int quantity = order.getQuantity();
                double price = item.getPrice();
                double subtotal = order.getSubtotal();
                try {
                    writer.write(item.getName() + " - " + quantity + " x Rp " + price + " = Rp " + subtotal + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.write("---------------------------------------+");
            writer.write("\nTotal Pembayaran: Rp " + total);
            writer.close();
            System.out.println("Struk pembayaran telah disimpan sebagai file struk_pembayaran.txt");
        } catch (Exception e) {
            System.err.println("Terjadi kesalahan dalam menyimpan struk pembayaran.");
        }
    }

    public void cancelOrder() {
        if (orders.isEmpty()) {
            System.out.println("Belum ada pesanan untuk dibatalkan.");
            return;
        }
        displayCurrentOrders();
        Optional<Integer> orderNumber = getUserInput("Masukkan nomor pesanan yang ingin dibatalkan: ");
        orderNumber.ifPresent(num -> {
            String[] orderKeys = orders.keySet().toArray(new String[0]);
            if (num >= 1 && num <= orderKeys.length) {
                String selectedOrderKey = orderKeys[num - 1];
                Order selectedOrder = orders.get(selectedOrderKey);
                MenuItem selectedItem = selectedOrder.getItem();
                int currentQuantity = selectedOrder.getQuantity();
                System.out.println("Pesanan yang Anda pilih: " + selectedItem.getName() + " - " + currentQuantity + " porsi");
                Optional<Integer> cancelQuantity = getUserInput("Masukkan jumlah porsi yang ingin dibatalkan: ");
                cancelQuantity.ifPresent(qty -> {
                    if (qty > 0 && qty <= currentQuantity) {
                        boolean cancelConfirmed = getConfirmation("Apakah Anda yakin ingin membatalkan " + qty + " porsi dari pesanan ini?");
                        if (cancelConfirmed) {
                            if (qty == currentQuantity) {
                                orders.remove(selectedOrderKey);
                            } else {
                                selectedOrder.setQuantity(currentQuantity - qty);
                            }
                            System.out.println("Pembatalan pesanan berhasil.");
                        } else {
                            System.out.println("Batal membatalkan pesanan.");
                        }
                    } else {
                        System.out.println("Jumlah porsi tidak valid.");
                    }
                });
            } else {
                System.out.println("Nomor pesanan tidak valid.");
            }
        });
    }

    private void displayCurrentOrders() {
        System.out.println("\n======= Pesanan Saat Ini =======");
        int index = 1;
        for (Order order : orders.values()) {
            MenuItem item = order.getItem();
            int quantity = order.getQuantity();
            System.out.println(index + ". " + item.getName() + " - " + quantity + " porsi");
            index++;
        }
        System.out.println("=================================");
    }

    private void clearOrders() {
        orders.clear();
    }
}

class MenuItem {
    private String name;
    private double price;

    public MenuItem(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}

class Order {
    private MenuItem item;
    private int quantity;

    public Order(MenuItem item, int quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public MenuItem getItem() {
        return item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getSubtotal() {
        return item.getPrice() * quantity;
    }
}

