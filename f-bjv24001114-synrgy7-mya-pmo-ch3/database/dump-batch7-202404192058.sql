PGDMP      :                |            batch7    16.1    16.1 %               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    16584    batch7    DATABASE     }   CREATE DATABASE batch7 WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_Indonesia.1252';
    DROP DATABASE batch7;
                postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                pg_database_owner    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   pg_database_owner    false    5            �            1259    16590    barang_id_seq    SEQUENCE     v   CREATE SEQUENCE public.barang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.barang_id_seq;
       public          postgres    false    5            �            1259    16630    merchant    TABLE     �   CREATE TABLE public.merchant (
    id uuid NOT NULL,
    merchant_name character varying(255),
    merchant_location character varying(255),
    open boolean
);
    DROP TABLE public.merchant;
       public         heap    postgres    false    5            �            1259    16673    merchant_id_seq    SEQUENCE     x   CREATE SEQUENCE public.merchant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.merchant_id_seq;
       public          postgres    false    5            �            1259    16647    orders    TABLE     �   CREATE TABLE public.orders (
    id uuid NOT NULL,
    order_time timestamp without time zone,
    destination_address character varying(255),
    user_id uuid,
    completed boolean
);
    DROP TABLE public.orders;
       public         heap    postgres    false    5            �            1259    16657    orders_detail    TABLE     �   CREATE TABLE public.orders_detail (
    id uuid NOT NULL,
    order_id uuid,
    product_id uuid,
    quantity integer,
    total_price numeric(10,2)
);
 !   DROP TABLE public.orders_detail;
       public         heap    postgres    false    5            �            1259    16676    orders_detail_id_seq    SEQUENCE     }   CREATE SEQUENCE public.orders_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.orders_detail_id_seq;
       public          postgres    false    5            �            1259    16675    orders_id_seq    SEQUENCE     v   CREATE SEQUENCE public.orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.orders_id_seq;
       public          postgres    false    5            �            1259    16637    product    TABLE     �   CREATE TABLE public.product (
    id uuid NOT NULL,
    product_name character varying(255),
    price numeric(10,2),
    merchant_id uuid
);
    DROP TABLE public.product;
       public         heap    postgres    false    5            �            1259    16674    product_id_seq    SEQUENCE     w   CREATE SEQUENCE public.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.product_id_seq;
       public          postgres    false    5            �            1259    16623    users    TABLE     �   CREATE TABLE public.users (
    id uuid NOT NULL,
    username character varying(255),
    email_address character varying(255),
    password character varying(255)
);
    DROP TABLE public.users;
       public         heap    postgres    false    5            �            1259    16672    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          postgres    false    5                      0    16630    merchant 
   TABLE DATA           N   COPY public.merchant (id, merchant_name, merchant_location, open) FROM stdin;
    public          postgres    false    218   }'                 0    16647    orders 
   TABLE DATA           Y   COPY public.orders (id, order_time, destination_address, user_id, completed) FROM stdin;
    public          postgres    false    220   #(                 0    16657    orders_detail 
   TABLE DATA           X   COPY public.orders_detail (id, order_id, product_id, quantity, total_price) FROM stdin;
    public          postgres    false    221   )                 0    16637    product 
   TABLE DATA           G   COPY public.product (id, product_name, price, merchant_id) FROM stdin;
    public          postgres    false    219   �)                 0    16623    users 
   TABLE DATA           F   COPY public.users (id, username, email_address, password) FROM stdin;
    public          postgres    false    217   �*                  0    0    barang_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.barang_id_seq', 5, true);
          public          postgres    false    216                        0    0    merchant_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.merchant_id_seq', 1, false);
          public          postgres    false    223            !           0    0    orders_detail_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.orders_detail_id_seq', 1, false);
          public          postgres    false    226            "           0    0    orders_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.orders_id_seq', 1, false);
          public          postgres    false    225            #           0    0    product_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.product_id_seq', 1, false);
          public          postgres    false    224            $           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 1, false);
          public          postgres    false    222            s           2606    16636    merchant merchant_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.merchant
    ADD CONSTRAINT merchant_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.merchant DROP CONSTRAINT merchant_pkey;
       public            postgres    false    218            y           2606    16661     orders_detail orders_detail_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.orders_detail
    ADD CONSTRAINT orders_detail_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.orders_detail DROP CONSTRAINT orders_detail_pkey;
       public            postgres    false    221            w           2606    16651    orders orders_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.orders DROP CONSTRAINT orders_pkey;
       public            postgres    false    220            u           2606    16641    product product_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.product DROP CONSTRAINT product_pkey;
       public            postgres    false    219            q           2606    16629    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    217            |           2606    16662 )   orders_detail orders_detail_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.orders_detail
    ADD CONSTRAINT orders_detail_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.orders(id);
 S   ALTER TABLE ONLY public.orders_detail DROP CONSTRAINT orders_detail_order_id_fkey;
       public          postgres    false    220    221    4727            }           2606    16667 +   orders_detail orders_detail_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.orders_detail
    ADD CONSTRAINT orders_detail_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id);
 U   ALTER TABLE ONLY public.orders_detail DROP CONSTRAINT orders_detail_product_id_fkey;
       public          postgres    false    219    4725    221            {           2606    16652    orders orders_user_id_fkey    FK CONSTRAINT     y   ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);
 D   ALTER TABLE ONLY public.orders DROP CONSTRAINT orders_user_id_fkey;
       public          postgres    false    220    4721    217            z           2606    16642     product product_merchant_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_merchant_id_fkey FOREIGN KEY (merchant_id) REFERENCES public.merchant(id);
 J   ALTER TABLE ONLY public.product DROP CONSTRAINT product_merchant_id_fkey;
       public          postgres    false    219    218    4723               �   x�-�91@�:9E.`�8!K	�!:(i����f�/F��~��C%�I �6B�H�Y���PP����n��a�9��6��Z5J���A���q�|��X�H�Y��z�;���D��	�e(> x�[Œ!&ik�^쎗?x��)o��Ik�L�3j         �   x����mCAC��W�d�h�ŷt�r�FR` �!p�� �H���G���!�3޲�t![jd(u^��PnU�Hĥ?����3�����C��zҘ�s"3P�G1��T����P�%�j4��ڱ�Q��C㑸�7 �
��]@	y_����C�]�ev����K�&i������	p�wK�%���U���~�u=���V`�         �   x���q0��\�$��H���d�k��j�f
�ް�8ц8���C���j�:p��._駱WLf��!����Y:W �L`��"?8x0��s6�9��Y�R�t�L�V����� ��=�I�@���Q�h�@��H�M�A�Tx%��A��og��Ԯ��~��n@���k�h+���m��22�NH��H�!J��&��⍐�fS��`�s����|?��/��[�         �   x��лm0 �ڞ"p�c�EH��iRD����D��:����Bs+��mG�պ +�46�����+~>ߨ������0$瀆�072O�s�-�w�/%4nO��s������~�\��p�w�Hpŀ�ä����,��',E��9r

��C[)`m8.�4�^Mb�$�6"h&	����1)�p+��+G�a�߷�g�隠�Ax7����Qk�Hs\�         �   x�m�;�0 Й�ň�&���b��TU��+QF�7>G*�ѠV$`�"��p�9w����>|�|�W���c[�a�����E�бqN�@Z��M �����gqy񜂊*s&����C�:C2k��d�|��6ʥ��B�.Co     