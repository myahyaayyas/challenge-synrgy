package com.example.challange4.service.product;

import com.example.challange4.entity.Merchant;
import com.example.challange4.entity.Product;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public interface ProductService {

    Map save(Product product);

    Map deleteProduct(UUID uuid);

    Map findProductById(UUID uuid);

    Map updateProduct(UUID uuid, Product request);

    Map findAllProduct(Pageable pageable);

}
