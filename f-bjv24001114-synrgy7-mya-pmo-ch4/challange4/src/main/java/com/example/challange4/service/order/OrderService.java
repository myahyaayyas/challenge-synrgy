package com.example.challange4.service.order;

import com.example.challange4.entity.Order;
import org.springframework.data.domain.Pageable;

import java.util.Map;
import java.util.UUID;

public interface OrderService {

    Map save(Order order);

    Map delete(UUID uuid);

    Map findById(UUID uuid);

    Map findAll(Pageable pageable);

    Map update(UUID uuid, Order request);
}
