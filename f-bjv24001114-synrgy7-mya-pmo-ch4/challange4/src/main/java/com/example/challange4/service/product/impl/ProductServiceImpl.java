package com.example.challange4.service.product.impl;

import com.example.challange4.entity.Merchant;
import com.example.challange4.entity.Product;
import com.example.challange4.repository.MerchantRepository;
import com.example.challange4.service.product.ProductService;
import com.example.challange4.repository.ProductRepository;
import com.example.challange4.utils.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private MerchantRepository merchantRepository;

    @Autowired
    private Response response;

    @Override
    public Map save(Product product) {
        return response.sukses(productRepository.save(product));
    }

    @Override
    public Map deleteProduct(UUID uuid) {
        Optional<Product> product = productRepository.findById(uuid);
        if (!product.isPresent()) {
            return response.error("Product ID Not Found", HttpStatus.NOT_FOUND);
        }
        productRepository.delete(product.get());
        return response.sukses(product.get());
    }

    @Override
    public Map findProductById(UUID uuid) {
        Optional<Product> product = productRepository.findById(uuid);
        if (!product.isPresent()) {
            return response.error("Product ID Not Found", HttpStatus.NOT_FOUND);
        }
        return response.sukses(product.get());
    }

    @Override
    public Map updateProduct(UUID uuid, Product request) {
        Optional<Product> productOptional = productRepository.findById(uuid);
        if (!productOptional.isPresent()) {
            return response.error("Product ID Not Found", HttpStatus.NOT_FOUND);
        }

        Product product = productOptional.get();
        product.setProductName(request.getProductName());
        product.setPrice(request.getPrice());

        return response.sukses(productRepository.save(product));
    }

    @Override
    public Map findAllProduct(Pageable pageable) {
        return response.sukses(productRepository.findAll(pageable).getContent());
    }

}

