package com.example.challange4.service.user.impl;

import com.example.challange4.entity.User;
import com.example.challange4.repository.UserRepository;
import com.example.challange4.service.user.UserService;
import com.example.challange4.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Response response;

    @Override
    public Map save(User user) {
        return response.sukses(userRepository.save(user));
    }

    @Override
    public Map delete(UUID uuid) {
        Optional<User> user = userRepository.findById(uuid);
        if (!user.isPresent()) {
            return response.error("User ID Not Found", HttpStatus.NOT_FOUND);
        }
        userRepository.delete(user.get());
        return response.sukses(user.get());
    }

    @Override
    public Map findById(UUID uuid) {
        Optional<User> user = userRepository.findById(uuid);
        if (!user.isPresent()) {
            return response.error("User ID Not Found", HttpStatus.NOT_FOUND);
        }
        return response.sukses(user.get());
    }

    @Override
    public Map findAll(Pageable pageable) {
        return response.sukses(userRepository.findAll(pageable).getContent());
    }

    @Override
    public Map update(UUID uuid, User request) {
        Optional<User> user = userRepository.findById(uuid);
        if (!user.isPresent()) {
            return response.error("User ID Not Found", HttpStatus.NOT_FOUND);
        }

        user.get().setUsername(request.getUsername());
        user.get().setEmailAddress(request.getEmailAddress());
        user.get().setPassword(request.getPassword());

        return response.sukses(user.get());
    }
}