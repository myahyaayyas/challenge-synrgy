package com.example.challange4.controller;

import com.example.challange4.entity.User;
import com.example.challange4.repository.UserRepository;
import com.example.challange4.service.user.UserService;
import com.example.challange4.utils.Response;
import jakarta.persistence.criteria.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Response response;

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<?> create(@RequestBody User user) {
        return ResponseEntity.ok(userService.save(user));
    }

    @DeleteMapping(value = {"/delete{id}", "/delete{id}/"})
    public ResponseEntity<?> delete(@PathVariable UUID id) {
        return ResponseEntity.ok(userService.delete(id));
    }

    @PutMapping(value = {"/update{id}", "/update{id}/"})
    public ResponseEntity<?> edit(@PathVariable UUID id, @RequestBody User user) {
        return ResponseEntity.ok(userService.update(id, user));
    }

    @GetMapping()
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "0") Integer page,
                                     @RequestParam(defaultValue = "5") Integer size) {
        return ResponseEntity.ok(userService.findAll(PageRequest.of(page, size)));
    }

    @GetMapping(value = {"/{id}", "/{id}"})
    public ResponseEntity<?> findById(@PathVariable UUID id) {
        return ResponseEntity.ok(userService.findById(id));
    }

    @GetMapping(value = { "/list-user", "/list-user/" })
    public ResponseEntity<Map> list(@RequestParam(required = false, name = "username") String username,
                                    @RequestParam(required = false, name = "emailAddress") String emailAddress,
                                    @RequestParam(required = false, name = "password") String password,
                                    @PageableDefault(page = 0, size = 10) Pageable pageable) {


        Specification<User> spec = ((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (username != null && !username.isEmpty()) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("username")), "%" + username.toLowerCase() + "%"));
            }
            if (emailAddress != null && !emailAddress.isEmpty()) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("emailAddress")), "%" + emailAddress.toLowerCase() + "%"));
            }
            if (password != null && !password.isEmpty()) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("password")), "%" + password.toLowerCase() + "%"));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        });

        Page<User> clientCompanyList = userRepository.findAll(spec, pageable);
        return new ResponseEntity<Map>(response.sukses(clientCompanyList), HttpStatus.OK);
    }

}

