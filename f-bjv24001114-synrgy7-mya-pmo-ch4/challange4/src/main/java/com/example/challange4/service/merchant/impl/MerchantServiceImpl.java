package com.example.challange4.service.merchant.impl;

import com.example.challange4.entity.Merchant;
import com.example.challange4.service.merchant.MerchantService;
import com.example.challange4.repository.MerchantRepository;
import com.example.challange4.utils.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class MerchantServiceImpl implements MerchantService {

    @Autowired
    private MerchantRepository merchantRepository;

    @Autowired
    private Response response;

    @Override
    public Map save(Merchant merchant) {
        return response.sukses(merchantRepository.save(merchant));
    }

    @Override
    public Map delete(UUID uuid) {
        Optional<Merchant> merchant = merchantRepository.findById(uuid);
        if (!merchant.isPresent()) {
            return response.error("Merchant ID Not Found", HttpStatus.NOT_FOUND);
        }
        merchantRepository.delete(merchant.get());
        return response.sukses(merchant.get());
    }

    @Override
    public Map findById(UUID uuid) {
        Optional<Merchant> merchant = merchantRepository.findById(uuid);
        if (!merchant.isPresent()) {
            return response.error("Merchant ID Not Found", HttpStatus.NOT_FOUND);
        }
        return response.sukses(merchant.get());
    }

    @Override
    public Map findAll(Pageable pageable) {
        return response.sukses(merchantRepository.findAll(pageable).getContent());
    }

    @Override
    public Map update(UUID uuid, Merchant request) {
        Optional<Merchant> merchant = merchantRepository.findById(uuid);
        if (!merchant.isPresent()) {
            return response.error("Merchant ID Not Found", HttpStatus.NOT_FOUND);
        }

        merchant.get().setName(request.getName());
        merchant.get().setLocation(request.getLocation());
        merchant.get().setOpen(request.getOpen());

        return response.sukses(merchant.get());
    }
}

