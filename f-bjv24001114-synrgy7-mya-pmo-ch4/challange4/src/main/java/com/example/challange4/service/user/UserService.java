package com.example.challange4.service.user;

import com.example.challange4.entity.User;
import org.springframework.data.domain.Pageable;

import java.util.Map;
import java.util.UUID;

public interface UserService {

    Map save(User user);

    Map delete(UUID uuid);

    Map findById(UUID uuid);

    Map findAll(Pageable pageable);

    Map update(UUID uuid, User request);
}
