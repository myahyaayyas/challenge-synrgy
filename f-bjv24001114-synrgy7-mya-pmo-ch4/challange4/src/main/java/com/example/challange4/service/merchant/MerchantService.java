package com.example.challange4.service.merchant;

import com.example.challange4.entity.Merchant;
import org.springframework.data.domain.Pageable;

import java.util.Map;
import java.util.UUID;

public interface MerchantService {
    Map save(Merchant merchant);

    Map delete(UUID uuid);

    Map findById(UUID uuid);

    Map findAll(Pageable pageable);

    Map update(UUID uuid, Merchant request);
}

