package com.example.challange4.service.order.impl;

import com.example.challange4.entity.Order;
import com.example.challange4.service.order.OrderService;
import com.example.challange4.repository.OrderRepository;
import com.example.challange4.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private Response response;

    @Override
    public Map save(Order order) {
        return response.sukses(orderRepository.save(order));
    }

    @Override
    public Map delete(UUID uuid) {
        Optional<Order> order = orderRepository.findById(uuid);
        if (!order.isPresent()) {
            return response.error("Order ID Not Found", HttpStatus.NOT_FOUND);
        }
        orderRepository.delete(order.get());
        return response.sukses(order.get());
    }

    @Override
    public Map findById(UUID uuid) {
        Optional<Order> order = orderRepository.findById(uuid);
        if (!order.isPresent()) {
            return response.error("Order ID Not Found", HttpStatus.NOT_FOUND);
        }
        return response.sukses(order.get());
    }

    @Override
    public Map findAll(Pageable pageable) {
        return response.sukses(orderRepository.findAll(pageable).getContent());
    }

    @Override
    public Map update(UUID uuid, Order request) {
        Optional<Order> order = orderRepository.findById(uuid);
        if (!order.isPresent()) {
            return response.error("Order ID Not Found", HttpStatus.NOT_FOUND);
        }

        order.get().setOrderTime(request.getOrderTime());
        order.get().setDestinationAddress(request.getDestinationAddress());
        order.get().setCompleted(request.getCompleted());

        return response.sukses(order.get());
    }
}
