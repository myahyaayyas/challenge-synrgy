package com.example.challange4.controller;

import com.example.challange4.entity.Merchant;
import com.example.challange4.repository.MerchantRepository;
import com.example.challange4.service.merchant.MerchantService;

import com.example.challange4.utils.Response;
import jakarta.persistence.criteria.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/merchants")
public class MerchantController {

    @Autowired
    private Response response;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private MerchantRepository merchantRepository;

    @DeleteMapping(value = {"/delete{id}", "/delete{id}/"})
    public ResponseEntity<?> delete(@PathVariable UUID id) {
        return ResponseEntity.ok(merchantService.delete(id));
    }

    @PutMapping(value = {"/update{id}", "/update{id}/"})
    public ResponseEntity<?> edit(@PathVariable UUID id, @RequestBody Merchant merchant) {
        return ResponseEntity.ok(merchantService.update(id, merchant));
    }

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<?> create(@RequestBody Merchant merchant) {
        return ResponseEntity.ok(merchantService.save(merchant));
    }

    @GetMapping()
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "0") Integer page,
                                     @RequestParam(defaultValue = "5") Integer size) {
        return ResponseEntity.ok(merchantService.findAll(PageRequest.of(page, size)));
    }

    @GetMapping(value = {"/{id}", "/{id}"})
    public ResponseEntity<?> findById(@PathVariable UUID id) {
        return ResponseEntity.ok(merchantService.findById(id));
    }

    @GetMapping(value = { "/list-merchant", "/list-merchant/" })
    public ResponseEntity<Map> list(@RequestParam(required = false, name = "name") String name,
                                    @RequestParam(required = false, name = "location") String location,
                                    @RequestParam(required = false, name = "open") Boolean open,
                                    @PageableDefault(page = 0, size = 10) Pageable pageable) {


        Specification<Merchant> spec = ((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (name != null && !name.isEmpty()) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + name.toLowerCase() + "%"));
            }
            if (location != null && !location.isEmpty()) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("location")), "%" + location.toLowerCase() + "%"));
            }
            if (open != null) {
                predicates.add(criteriaBuilder.equal(root.get("open"), open));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        });

        Page<Merchant> clientCompanyList = merchantRepository.findAll(spec, pageable);
        return new ResponseEntity<Map>(response.sukses(clientCompanyList), HttpStatus.OK);
    }

}

