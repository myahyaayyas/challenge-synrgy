package com.example.challange.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(securedEnabled = true)
public class Oauth2ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        super.configure(resources);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/showFile/**", "/v1/showFile/**", "/v1/upload", "/v1/user-register/**", "/swagger-ui/**", "/swagger-ui.html", "/v3/api-docs/**", "/user-login/**",
                        "/forget-password/**", "/oauth/authorize**", "/login**", "/error**")
                .permitAll()
               // --------------------------- setting privilage ---------------------------------
//                .antMatchers("/v1/role-test-global/list-produk").hasAnyAuthority("ROLE_READ")
//                .antMatchers("/v1/role-test-global/post-produk").hasAnyAuthority("ROLE_WRITE")
//                .antMatchers("/v1/role-test-global/post-produk-admin").hasAnyAuthority("ROLE_ADMIN")
                .antMatchers("/api/v1/users/save").hasAnyAuthority("ROLE_USER")
                .antMatchers("/api/v1/users/delete/{id}").hasAnyAuthority("ROLE_USER")
                .antMatchers("/api/v1/users/update/{id}}").hasAnyAuthority("ROLE_USER")
                .antMatchers("/api/v1/merchants").hasAnyAuthority("ROLE_USER")
                .antMatchers("/api/v1/products").hasAnyAuthority("ROLE_USER")
                .anyRequest().authenticated()
                .and()
                .formLogin().permitAll();
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
