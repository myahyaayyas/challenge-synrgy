package com.example.challange.service.merchant.impl;

import com.example.challange.entity.Merchant;
import com.example.challange.entity.Product;
import com.example.challange.repository.ProductRepository;
import com.example.challange.service.merchant.MerchantService;
import com.example.challange.repository.MerchantRepository;
import com.example.challange.utils.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class MerchantServiceImpl implements MerchantService {

    @Autowired
    private MerchantRepository merchantRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private Response response;

    @Override
    public Map save(Merchant merchant) {
        return response.sukses(merchantRepository.save(merchant));
    }

    @Override
    public Map deleteMerchant(UUID id) {
        Optional<Merchant> existingMerchantOptional = merchantRepository.findById(id);

        if (existingMerchantOptional.isPresent()) {
            Merchant merchant = existingMerchantOptional.get();
            merchant.setDeleted_date(new Date());
            merchantRepository.save(merchant);

            List<Product> products = productRepository.findByMerchantId(id);
            for (Product product : products) {
                product.setDeleted_date(new Date());
                productRepository.save(product);
            }

            return Map.of("message", "Merchant and related products marked as deleted successfully");
        } else {
            return Map.of("error", "Merchant not found with id " + id);
        }
    }

    @Override
    public Map findById(UUID uuid) {
        Optional<Merchant> merchant = merchantRepository.findById(uuid);
        if (!merchant.isPresent()) {
            return response.error("Merchant ID Not Found", HttpStatus.NOT_FOUND);
        }
        return response.sukses(merchant.get());
    }

    @Override
    public Map findAll(Pageable pageable) {
        return response.sukses(merchantRepository.findAll(pageable).getContent());
    }

    @Override
    public Map findByOpen(Boolean open) {
        List<Merchant> merchants = merchantRepository.findByOpen(open);
        return response.sukses(merchants);
    }

    @Override
    public Map updateMerchant(UUID id, Merchant merchant) {
        Optional<Merchant> existingMerchantOptional = merchantRepository.findById(id);

        if (existingMerchantOptional.isPresent()) {
            Merchant existingMerchant = existingMerchantOptional.get();
            existingMerchant.setName(merchant.getName());
            existingMerchant.setLocation(merchant.getLocation());
            existingMerchant.setOpen(merchant.getOpen());

            Merchant updatedMerchant = merchantRepository.save(existingMerchant);
            return response.sukses(updatedMerchant);
        } else {
            return response.error("Merchant not found with id " + id, 404);
        }
    }


}

