package com.example.challange.testing;


import com.example.challange.entity.Merchant;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class Testing {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void listSuksesMerchant() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "*/*");
        headers.set("Content-Type", "application/json");


        ResponseEntity<Object> exchange = restTemplate.exchange("http://localhost:8082/api/v1/merchants", HttpMethod.GET, null, Object.class);
        System.out.println("response  =" + exchange.getBody());
    }

    @Test
    public void listSuksesJSONObject() {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> exchange = restTemplate.exchange(
                "http://localhost:8082/api/v1/merchants/list-merchant/",
                HttpMethod.GET,
                null,
                String.class
        );

        String responseBody = exchange.getBody();
        if (responseBody != null) {
            try {
                JSONObject jsonResponse = new JSONObject(responseBody);
                JSONArray dataArray = jsonResponse.getJSONArray("data");

                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject merchantObject = dataArray.getJSONObject(i);
                    UUID id = UUID.fromString(merchantObject.getString("id"));
                    String name = merchantObject.getString("name");
                    String location = merchantObject.getString("location");
                    boolean open = merchantObject.getBoolean("open");

                    Merchant merchant = new Merchant();
                    merchant.setId(id);
                    merchant.setName(name);
                    merchant.setLocation(location);
                    merchant.setOpen(open);

                    System.out.println("Merchant: " + merchant);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
