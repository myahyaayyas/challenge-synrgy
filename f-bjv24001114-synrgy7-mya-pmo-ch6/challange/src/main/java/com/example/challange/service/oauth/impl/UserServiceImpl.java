package com.example.challange.service.oauth.impl;

import com.example.challange.config.Config;
import com.example.challange.entity.oauth.LoginModel;
import com.example.challange.entity.Order;
import com.example.challange.entity.oauth.RegisterModel;
import com.example.challange.entity.oauth.Role;
import com.example.challange.entity.oauth.User;
import com.example.challange.repository.OrderRepository;
import com.example.challange.repository.oauth.RoleRepository;
import com.example.challange.repository.oauth.UserRepository;
import com.example.challange.service.oauth.UserService;
import com.example.challange.utils.Response;
import com.itextpdf.text.log.Logger;
import com.itextpdf.text.log.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    Config config = new Config();

    @Value("${BASEURL}")
    private String baseUrl;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private Response response;

    @Autowired
    private RoleRepository repoRole;

    @Autowired
    UserRepository repoUser;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;


    @Autowired
    private PasswordEncoder encoder;

    @Override
    public Map save(User user) {
        return response.sukses(userRepository.save(user));
    }

    @Override
    public Map deleteUsers(UUID id) {
        Optional<User> existingUserOptional = userRepository.findById(id);

        if (existingUserOptional.isPresent()) {
            List<Order> orders = orderRepository.findByUsersId(id);
            orderRepository.deleteAll(orders);

            userRepository.deleteById(id);
            return response.sukses("User and related orders deleted successfully");
        } else {
            return response.error("User not found with id " + id, 404);
        }
    }

    @Override
    public Map findById(UUID uuid) {
        Optional<User> user = userRepository.findById(uuid);
        if (!user.isPresent()) {
            return response.error("User ID Not Found", HttpStatus.NOT_FOUND);
        }
        return response.sukses(user.get());
    }

    @Override
    public Map findAll(Pageable pageable) {

        return response.sukses(userRepository.findAll(pageable).getContent());
    }

    @Override
    public Map update(UUID id, User user) {
        Optional<User> existingUserOptional = userRepository.findById(id);

        if (existingUserOptional.isPresent()) {
            User existingUser = existingUserOptional.get();
            existingUser.setUsername(user.getUsername());
//            existingUser.setEmailAddress(user.getEmailAddress());
            existingUser.setPassword(user.getPassword());

            User updatedUser = userRepository.save(existingUser);
            return response.sukses(updatedUser);
        } else {
            return response.error("User not found with id " + id, 404);
        }
    }

    @Override
    public Map registerManual(RegisterModel objModel) {
        Map map = new HashMap();
        try {
            String[] roleNames = {"ROLE_USER", "ROLE_READ", "ROLE_WRITE"}; // admin
            User user = new User();
            user.setUsername(objModel.getUsername().toLowerCase());
            user.setFullname(objModel.getFullname());

            //step 1 :
//            user.setEnabled(false); // matikan user

            String password = encoder.encode(objModel.getPassword().replaceAll("\\s+", ""));
            List<Role> r = repoRole.findByNameIn(roleNames);

            user.setRoles(r);
            user.setPassword(password);
            User obj = repoUser.save(user);

            return response.sukses(obj);

        } catch (Exception e) {
            logger.error("Eror registerManual=", e);
            return response.error("eror:", e);
        }

    }

    @Override
    public Map registerByGoogle(RegisterModel objModel) {
        Map map = new HashMap();
        try {
            String[] roleNames = {"ROLE_USER", "ROLE_READ", "ROLE_WRITE"}; // ROLE DEFAULE
            User user = new User();
            user.setUsername(objModel.getUsername().toLowerCase());
            user.setFullname(objModel.getFullname());
            //step 1 :
            user.setEnabled(false); // matikan user
            String password = encoder.encode(objModel.getPassword().replaceAll("\\s+", ""));
            List<Role> r = repoRole.findByNameIn(roleNames);
            user.setRoles(r);
            user.setPassword(password);
            User obj = repoUser.save(user);
            return response.sukses(obj);

        } catch (Exception e) {
            logger.error("Eror registerManual=", e);
            return response.error("eror:", e);
        }
    }

    @Override
    public Map login(LoginModel loginModel) {
        /**
         * bussines logic for login here
         * **/
        try {
            Map<String, Object> map = new HashMap<>();

            User checkUser = userRepository.findOneByUsername(loginModel.getUsername());

            if ((checkUser != null) && (encoder.matches(loginModel.getPassword(), checkUser.getPassword()))) {
                if (!checkUser.isEnabled()) {
                    map.put("is_enabled", checkUser.isEnabled());
                    return response.error(map, 404);
                }
            }
            if (checkUser == null) {
                return response.notFound("user not found");
            }
            if (!(encoder.matches(loginModel.getPassword(), checkUser.getPassword()))) {
                return response.error("wrong password", 404);
            }
            String url = baseUrl + "/oauth/token?username=" + loginModel.getUsername() +
                    "&password=" + loginModel.getPassword() +
                    "&grant_type=password" +
                    "&client_id=my-client-web" +
                    "&client_secret=password";
            ResponseEntity<Map> response = restTemplateBuilder.build().exchange(url, HttpMethod.POST, null, new
                    ParameterizedTypeReference<Map>() {
                    });

            if (response.getStatusCode() == HttpStatus.OK) {
                User user = userRepository.findOneByUsername(loginModel.getUsername());
                List<String> roles = new ArrayList<>();

                for (Role role : user.getRoles()) {
                    roles.add(role.getName());
                }
                //save token
//                checkUser.setAccessToken(response.getBody().get("access_token").toString());
//                checkUser.setRefreshToken(response.getBody().get("refresh_token").toString());
//                userRepository.save(checkUser);

                map.put("access_token", response.getBody().get("access_token"));
                map.put("token_type", response.getBody().get("token_type"));
                map.put("refresh_token", response.getBody().get("refresh_token"));
                map.put("expires_in", response.getBody().get("expires_in"));
                map.put("scope", response.getBody().get("scope"));
                map.put("jti", response.getBody().get("jti"));

                return map;
            } else {
//                return response.notFound("user not found");
            }
        } catch (HttpStatusCodeException e) {
            e.printStackTrace();
            if (e.getStatusCode() == HttpStatus.BAD_REQUEST) {
                return response.templateEror("invalid login");
            }
            return response.error(e, 404);
        } catch (Exception e) {
            e.printStackTrace();

            return response.error(e, 404);
        }
        return Map.of();
    }

}