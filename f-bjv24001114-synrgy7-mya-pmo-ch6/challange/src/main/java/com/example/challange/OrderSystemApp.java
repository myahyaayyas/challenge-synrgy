package com.example.challange;

import com.example.challange.controller.fileupload.FileStorageProperties;
//import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
//@OpenAPIDefinition
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class OrderSystemApp {

//    private static final Logger logger = LoggerFactory.getLogger(OrderSystemApp.class);

    public static void main(String[] args) {
        SpringApplication.run(OrderSystemApp.class, args);
//        logger.info("info logging level");
//        logger.error("eror logging level");
//        logger.warn("warning logging level");
//        logger.debug("debug logging level");
//        logger.trace("trace logging level");
//        logger.error("Error processing request");
    }
}
