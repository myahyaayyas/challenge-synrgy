package com.example.challange.service.product.impl;

import com.example.challange.entity.Merchant;
import com.example.challange.entity.Product;
import com.example.challange.repository.MerchantRepository;
import com.example.challange.service.product.ProductService;
import com.example.challange.repository.ProductRepository;
import com.example.challange.utils.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private MerchantRepository merchantRepository;

    @Autowired
    private Response response;

    @Override
    public Map save(Product product) {
        if (product.getMerchant() != null && product.getMerchant().getId() != null) {
            Optional<Merchant> merchantOpt = merchantRepository.findById(product.getMerchant().getId());
            if (merchantOpt.isPresent()) {
                product.setMerchant(merchantOpt.get());
            } else {
                return response.error("Merchant not found with id " + product.getMerchant().getId(), HttpStatus.NOT_FOUND);
            }
        }
        return response.sukses(productRepository.save(product));
    }

    @Override
    public Map deleteProduct(UUID id) {

        Optional<Product> existingProductOptional = productRepository.findById(id);

        if (existingProductOptional.isPresent()) {
            productRepository.deleteById(id);
            return response.sukses("Product deleted successfully");
        } else {
            return response.error("Product not found with id " + id, 404);
        }
    }

    @Override
    public Map findProductById(UUID id) {
        Optional<Product> product = productRepository.findById(id);
        if (!product.isPresent()) {
            return response.error("Product ID Not Found", HttpStatus.NOT_FOUND);
        }
        return response.sukses(product.get());
    }

    @Override
    public Map findByStock(Boolean stock) {
        List<Product> products = productRepository.findByStock(stock);
        return response.sukses(products);
    }

    @Override
    public Map updateProduct(UUID id, Product product) {
        Optional<Product> existingProductOptional = productRepository.findById(id);

        if (existingProductOptional.isPresent()) {
            Product existingProduct = existingProductOptional.get();
            existingProduct.setProductName(product.getProductName());
            existingProduct.setPrice(product.getPrice());
            existingProduct.setStock(product.getStock());

            Product updateProduct = productRepository.save(existingProduct);
            return response.sukses(updateProduct);
        } else {
            return response.error("Product not found with id " + id, 404);
        }
    }

    @Override
    public Map findAllProduct(Pageable pageable) {
        return response.sukses(productRepository.findAll(pageable).getContent());
    }

    @Override
    public List<Product> findProductByMerchantId(UUID merchantId) {
        return productRepository.findByMerchantId(merchantId);
    }

}

