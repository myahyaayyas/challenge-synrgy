package com.example.challange.service.order.impl;

import com.example.challange.entity.Order;
import com.example.challange.service.order.OrderService;
import com.example.challange.repository.OrderRepository;
import com.example.challange.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private Response response;

    @Override
    public Map save(Order order) {
        return response.sukses(orderRepository.save(order));
    }

    @Override
    public Map delete(UUID id) {
        Optional<Order> existingOrderOptional = orderRepository.findById(id);

        if (existingOrderOptional.isPresent()) {
            orderRepository.deleteById(id);
            return response.sukses("Order deleted successfully");
        } else {
            return response.error("Order not found with id " + id, 404);
        }
    }

    @Override
    public Map findById(UUID uuid) {
        Optional<Order> order = orderRepository.findById(uuid);
        if (!order.isPresent()) {
            return response.error("Order ID Not Found", HttpStatus.NOT_FOUND);
        }
        return response.sukses(order.get());
    }

    @Override
    public Map findAll(Pageable pageable) {
        return response.sukses(orderRepository.findAll(pageable).getContent());
    }

    @Override
    public Map update(UUID id, Order order) {
        Optional<Order> existingOrderOptional = orderRepository.findById(id);

        if (existingOrderOptional.isPresent()) {
            Order existingOrder = existingOrderOptional.get();
            existingOrder.setOrderTime(order.getOrderTime());
            existingOrder.setDestinationAddress(order.getDestinationAddress());
            existingOrder.setCompleted(order.getCompleted());

            Order updatedOrder = orderRepository.save(existingOrder);
            return response.sukses(updatedOrder);
        } else {
            return response.error("Order not found with id " + id, 404);
        }
    }

    @Override
    public List<Order> findOrderByUsersId(UUID usersId) {
        return orderRepository.findByUsersId(usersId);
    }
}
