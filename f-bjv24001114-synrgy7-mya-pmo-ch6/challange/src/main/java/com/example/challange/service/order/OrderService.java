package com.example.challange.service.order;

import com.example.challange.entity.Order;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface OrderService {

    Map save(Order order);

    Map delete(UUID id);

    Map findById(UUID id);

    Map findAll(Pageable pageable);

    Map update(UUID uuid, Order order);

    List findOrderByUsersId(UUID usersId);
}
