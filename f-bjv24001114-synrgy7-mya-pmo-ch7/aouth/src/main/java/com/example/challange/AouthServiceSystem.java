package com.example.challange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AouthServiceSystem {

    public static void main(String[] args) {
        SpringApplication.run(AouthServiceSystem.class, args);
    }
}
