package com.example.challange.service.oauth;

import com.example.challange.entity.oauth.LoginModel;
import com.example.challange.entity.oauth.RegisterModel;
import com.example.challange.entity.oauth.User;
import org.springframework.data.domain.Pageable;

import java.util.Map;
import java.util.UUID;

public interface UserService {

    Map save(User user);

    Map deleteUsers(UUID id);

    Map findById(UUID id);

    Map findAll(Pageable pageable);

    Map update(UUID uuid, User user);

    Map registerManual(RegisterModel objModel) ;

    Map registerByGoogle(RegisterModel objModel) ;

//    public Map login(LoginModel objLogin);

    Map login(LoginModel loginModel);
}
