package com.example.challange.controller;

import com.example.challange.entity.Order;
import com.example.challange.repository.OrderRepository;
import com.example.challange.service.order.OrderService;
import com.example.challange.service.order.impl.OrderServiceImpl;
import com.example.challange.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderServiceImpl orderServiceImpl;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private Response response;

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<?> create(@RequestBody Order order) {
        return ResponseEntity.ok(orderService.save(order));
    }

    @DeleteMapping(value = {"/delete/{id}", "/delete/{id}/"})
    public ResponseEntity<?> delete(@PathVariable UUID id) {
        Map response = orderService.delete(id);
        return ResponseEntity.ok(response);
    }

    @PutMapping(value = {"/update/{id}", "/update/{id}/"})
    public ResponseEntity<?> edit(@PathVariable UUID id, @RequestBody Order order) {

        return ResponseEntity.ok(orderService.update(id, order));
    }

    @GetMapping()
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "0") Integer page,
                                     @RequestParam(defaultValue = "5") Integer size) {
        return ResponseEntity.ok(orderService.findAll(PageRequest.of(page, size)));
    }

    @GetMapping(value = {"/{id}", "/{id}"})
    public ResponseEntity<?> findById(@PathVariable UUID id) {
        return ResponseEntity.ok(orderService.findById(id));
    }

    @PostMapping("/sendOrder")
    public void sendOrder(@RequestBody String order) {
        orderServiceImpl.sendOrderMessage(order);
    }

//    @GetMapping(value = {"/users/{usersId}", "/users/{usersId}/"})
//    public List findOrderByUsersId(@PathVariable UUID usersId) {
//        return orderService.findOrderByUsersId(usersId);
//    }

}
