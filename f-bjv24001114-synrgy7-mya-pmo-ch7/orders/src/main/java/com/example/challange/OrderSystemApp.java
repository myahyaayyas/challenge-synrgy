package com.example.challange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
public class OrderSystemApp {

    public static void main(String[] args) {
        SpringApplication.run(OrderSystemApp.class, args);
    }
}
