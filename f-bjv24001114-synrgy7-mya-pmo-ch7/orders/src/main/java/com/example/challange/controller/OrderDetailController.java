package com.example.challange.controller;


import com.example.challange.entity.OrderDetail;
import com.example.challange.service.orderDetail.OrderDetailService;
import com.example.challange.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/orderdetail")
public class OrderDetailController {

    @Autowired
    private OrderDetailService orderDetailService;

    @Autowired
    private Response response;

    @PostMapping(value = {"/create", "/create/"})
    public ResponseEntity<?> createOrderDetail(@RequestBody OrderDetail orderDetail) {
        Map<String, Object> response = orderDetailService.createOrderDetail(orderDetail);
        return ResponseEntity.status((int) response.get("status")).body(response);
    }

    @GetMapping()
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "0") Integer page,
                                     @RequestParam(defaultValue = "5") Integer size) {
        return ResponseEntity.ok(orderDetailService.findAll(PageRequest.of(page, size)));
    }

    @DeleteMapping(value = {"/delete/{id}", "/delete/{id}/"})
    public ResponseEntity<?> delete(@PathVariable UUID id) {
        Map response = orderDetailService.delete(id);
        return ResponseEntity.ok(response);
    }

//    @PutMapping(value = {"/update/{id}", "/update/{id}/"})
//    public ResponseEntity<?> edit(@PathVariable UUID id,
//                                  @RequestParam UUID productId,
//                                  @RequestParam Integer quantity) {
//
//        Map response = orderDetailService.update(id, productId, quantity);
//        return ResponseEntity.status((int) response.get("status")).body(response);
//    }

    @GetMapping(value = {"/{id}", "/{id}"})
    public ResponseEntity<?> findById(@PathVariable UUID id) {
        return ResponseEntity.ok(orderDetailService.findById(id));
    }
}
