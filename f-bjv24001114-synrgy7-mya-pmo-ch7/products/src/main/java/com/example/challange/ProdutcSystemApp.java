package com.example.challange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProdutcSystemApp {


    public static void main(String[] args) {
        SpringApplication.run(ProdutcSystemApp.class, args);
    }
}
