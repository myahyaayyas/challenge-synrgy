package com.example.challange.service.product;

import com.example.challange.entity.Product;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface ProductService {

    Map save(Product product);

    Map deleteProduct(UUID id);

    Map findProductById(UUID id);

    Map findByStock(Boolean stock);

    Map updateProduct(UUID id, Product request);

    Map findAllProduct(Pageable pageable);

    List<Product> findProductByMerchantId(UUID merchantId);

}
