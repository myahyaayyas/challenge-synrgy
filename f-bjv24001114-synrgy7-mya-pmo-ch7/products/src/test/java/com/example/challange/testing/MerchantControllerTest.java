package com.example.challange.testing;

import com.example.challange.controller.MerchantController;
import com.example.challange.entity.Merchant;
import com.example.challange.service.merchant.MerchantService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@WebMvcTest(MerchantController.class)
@Import(TestConfig.class)
public class MerchantControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MerchantService merchantService;

    @Autowired
    private ObjectMapper objectMapper;

    private Merchant merchant;

    @BeforeEach
    void setUp() {
        merchant = new Merchant();
        merchant.setId(UUID.randomUUID());
        merchant.setName("Test Merchant");
        merchant.setLocation("Test Location");
        merchant.setOpen(true);
    }

    @Test
    void shouldCreateMerchant() throws Exception {
//        Mockito.when(merchantService.save(Mockito.any(Merchant.class))).thenReturn(merchant);
        
        mockMvc.perform(post("/api/v1/merchants/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(merchant)))
                .andExpect(status().isOk())
                .andDo(print());
    }
}

