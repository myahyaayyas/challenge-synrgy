package com.example.challange.testing;

import com.example.challange.utils.Response;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {

    @Bean
    public Response response() {
        return new Response();
    }
}
