package com.example.challange5.service.user.impl;

import com.example.challange5.entity.Merchant;
import com.example.challange5.entity.Order;
import com.example.challange5.entity.Product;
import com.example.challange5.entity.User;
import com.example.challange5.repository.OrderRepository;
import com.example.challange5.repository.ProductRepository;
import com.example.challange5.repository.UserRepository;
import com.example.challange5.service.user.UserService;
import com.example.challange5.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private Response response;

    @Override
    public Map save(User user) {
        return response.sukses(userRepository.save(user));
    }

    @Override
    public Map deleteUsers(UUID id) {
        Optional<User> existingUserOptional = userRepository.findById(id);

        if (existingUserOptional.isPresent()) {
            List<Order> orders = orderRepository.findByUsersId(id);
            orderRepository.deleteAll(orders);

            userRepository.deleteById(id);
            return response.sukses("User and related orders deleted successfully");
        } else {
            return response.error("User not found with id " + id, 404);
        }
    }

    @Override
    public Map findById(UUID uuid) {
        Optional<User> user = userRepository.findById(uuid);
        if (!user.isPresent()) {
            return response.error("User ID Not Found", HttpStatus.NOT_FOUND);
        }
        return response.sukses(user.get());
    }

    @Override
    public Map findAll(Pageable pageable) {

        return response.sukses(userRepository.findAll(pageable).getContent());
    }

    @Override
    public Map update(UUID id, User user) {
        Optional<User> existingUserOptional = userRepository.findById(id);

        if (existingUserOptional.isPresent()) {
            User existingUser = existingUserOptional.get();
            existingUser.setUsername(user.getUsername());
            existingUser.setEmailAddress(user.getEmailAddress());
            existingUser.setPassword(user.getPassword());

            User updatedUser = userRepository.save(existingUser);
            return response.sukses(updatedUser);
        } else {
            return response.error("User not found with id " + id, 404);
        }
    }
}