package com.example.challange5.service.merchant;

import com.example.challange5.entity.Merchant;
import org.springframework.data.domain.Pageable;

import java.util.Map;
import java.util.UUID;

public interface MerchantService {
    Map save(Merchant merchant);

    Map deleteMerchant(UUID id);

    Map findById(UUID uuid);

    Map findAll(Pageable pageable);

    Map findByOpen(Boolean open);

    Map updateMerchant(UUID id, Merchant merchant);
}


