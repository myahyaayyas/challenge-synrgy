package com.example.challange5.controller.fileupload.merchant;


import com.example.challange5.entity.Merchant;
import com.example.challange5.repository.MerchantRepository;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.pdf.PdfDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

@Service
public class PdfService {

    @Autowired
    private MerchantRepository merchantRepository;

    public ByteArrayInputStream generatePdf() {
        List<Merchant> merchants = merchantRepository.findAll();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            PdfWriter writer = new PdfWriter(out);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);

            Table table = new Table(new float[]{1, 3, 3, 1});
            table.addHeaderCell(new Cell().add("ID"));
            table.addHeaderCell(new Cell().add("Name"));
            table.addHeaderCell(new Cell().add("Location"));
            table.addHeaderCell(new Cell().add("Open"));

            for (Merchant merchant : merchants) {
                table.addCell(new Cell().add(merchant.getId().toString()));
                table.addCell(new Cell().add(merchant.getName()));
                table.addCell(new Cell().add(merchant.getLocation()));
                table.addCell(new Cell().add(merchant.getOpen() ? "Yes" : "No"));
            }

            document.add(table);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ByteArrayInputStream(out.toByteArray());
    }
}

