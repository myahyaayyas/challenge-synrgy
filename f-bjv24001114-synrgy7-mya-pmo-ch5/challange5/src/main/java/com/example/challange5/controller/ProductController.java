package com.example.challange5.controller;

import com.example.challange5.entity.Product;
import com.example.challange5.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    @Autowired
    private ProductService productService;


    @DeleteMapping(value = {"/delete/{id}", "/delete/{id}/",})
    public ResponseEntity<?> deleteProduct(@PathVariable UUID id) {
        return ResponseEntity.ok(productService.deleteProduct(id));
    }

    @PutMapping(value = {"/update/{id}", "/update/{id}/"})
    public ResponseEntity<?> editProduct(@PathVariable UUID id, @RequestBody Product product) {
        return ResponseEntity.ok(productService.updateProduct(id, product));
    }

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<?> create(@RequestBody Product product) {
        return ResponseEntity.ok(productService.save(product));
    }

    @GetMapping(value = {"/stock", "/stock/"})
    public ResponseEntity<Map> getProductByStock(@RequestParam Boolean stock) {
        Map response = productService.findByStock(stock);
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = {"/{id}", "/{id}"})
    public ResponseEntity<?> findById(@PathVariable UUID id) {
        return ResponseEntity.ok(productService.findProductById(id));
    }

    @GetMapping()
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "0") Integer page,
                                     @RequestParam(defaultValue = "5") Integer size) {
        return ResponseEntity.ok(productService.findAllProduct(PageRequest.of(page, size)));
    }

    @GetMapping(value = {"/merchant/{merchantId}", "/merchant/{merchantId}/"})
    public List<Product> findProductByMerchantId(@PathVariable UUID merchantId) {
        return productService.findProductByMerchantId(merchantId);
    }
}
