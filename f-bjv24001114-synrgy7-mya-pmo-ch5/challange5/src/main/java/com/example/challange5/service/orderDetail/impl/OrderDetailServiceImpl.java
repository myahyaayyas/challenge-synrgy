package com.example.challange5.service.orderDetail.impl;

import com.example.challange5.entity.Order;
import com.example.challange5.entity.OrderDetail;
import com.example.challange5.entity.Product;
import com.example.challange5.repository.OrderDetailRepository;
import com.example.challange5.repository.OrderRepository;
import com.example.challange5.repository.ProductRepository;
import com.example.challange5.service.orderDetail.OrderDetailService;
import com.example.challange5.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private Response response;

    @Override
    public Map createOrderDetail(OrderDetail orderDetail) {
        try {
            Optional<Order> orderOptional = orderRepository.findById(orderDetail.getOrders().getId());
            if (!orderOptional.isPresent()) {
                return response.error("Order not found", 404);
            }

            Optional<Product> productOptional = productRepository.findById(orderDetail.getProduct().getId());
            if (!productOptional.isPresent()) {
                return response.error("Product not found", 404);
            }

            Order order = orderOptional.get();
            Product product = productOptional.get();

            if (product.getPrice() == null) {
                return response.error("Product price cannot be null", 400);
            }

            orderDetail.setOrders(order);
            orderDetail.setProduct(product);

            orderDetail.calculateTotalPrice();

            OrderDetail savedOrderDetail = orderDetailRepository.save(orderDetail);
            return response.sukses(savedOrderDetail);
        } catch (IllegalArgumentException e) {
            return response.error(e.getMessage(), 400);
        }
    }

    @Override
    public Map findAll(Pageable pageable) {
        return response.sukses(orderDetailRepository.findAll(pageable).getContent());
    }

    @Override
    public Map findById(UUID id) {
        Optional<OrderDetail> orderDetail = orderDetailRepository.findById(id);
        if (!orderDetail.isPresent()) {
            return response.error("Order ID Not Found", HttpStatus.NOT_FOUND);
        }
        return response.sukses(orderDetail.get());
    }

    @Override
    public Map delete(UUID id) {
        Optional<OrderDetail> existingOrderOptional = orderDetailRepository.findById(id);

        if (existingOrderOptional.isPresent()) {
            orderDetailRepository.deleteById(id);
            return response.sukses("Order deleted successfully");
        } else {
            return response.error("Order not found with id " + id, 404);
        }
    }

    @Override
    public Map update(UUID id, UUID productId, Integer quantity) {
        Optional<OrderDetail> existingOrderDetailOpt = orderDetailRepository.findById(id);
        if (!existingOrderDetailOpt.isPresent()) {
            return response.error("OrderDetail not found", 404);
        }

        OrderDetail existingOrderDetail = existingOrderDetailOpt.get();

        Optional<Product> productOptional = productRepository.findById(productId);
        if (!productOptional.isPresent()) {
            return response.error("Product not found", 404);
        }

        Product product = productOptional.get();

        if (product.getPrice() == null) {
            return response.error("Product price cannot be null", 400);
        }

        existingOrderDetail.setProduct(product);
        existingOrderDetail.setQuantity(quantity);
        existingOrderDetail.calculateTotalPrice();

        // Save updated order detail
        OrderDetail updatedOrderDetail = orderDetailRepository.save(existingOrderDetail);
        return response.sukses(updatedOrderDetail);
    }
}
