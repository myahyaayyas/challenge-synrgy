package com.example.challange5.controller;

import com.example.challange5.controller.fileupload.merchant.PdfService;
import com.example.challange5.entity.Merchant;
import com.example.challange5.repository.MerchantRepository;
import com.example.challange5.service.merchant.MerchantService;

import com.example.challange5.utils.Response;
import jakarta.persistence.criteria.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/merchants")
public class MerchantController {

    @Autowired
    private Response response;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private MerchantRepository merchantRepository;

    @DeleteMapping(value = {"/delete/{id}", "/delete/{id}/"})
    public ResponseEntity<?> delete(@PathVariable UUID id) {
        Map response = merchantService.deleteMerchant(id);
        return ResponseEntity.ok(response);
    }

    @PutMapping(value = {"/update/{id}", "/update/{id}/"})
    public ResponseEntity<?> edit(@PathVariable UUID id, @RequestBody Merchant merchant) {
        Map response = merchantService.updateMerchant(id, merchant);
        return ResponseEntity.ok(response);
    }

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<?> create(@RequestBody Merchant merchant) {

        return ResponseEntity.ok(merchantService.save(merchant));
    }

    @GetMapping()
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "0") Integer page,
                                     @RequestParam(defaultValue = "5") Integer size) {
        return ResponseEntity.ok(merchantService.findAll(PageRequest.of(page, size)));
    }

    @GetMapping(value = {"/{id}", "/{id}"})
    public ResponseEntity<?> findById(@PathVariable UUID id) {
        return ResponseEntity.ok(merchantService.findById(id));
    }

    @GetMapping(value = {"/open", "/open/"})
    public ResponseEntity<Map> getMerchantsByOpen(@RequestParam Boolean open) {
        Map response = merchantService.findByOpen(open);
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = { "/list-merchant", "/list-merchant/" })
    public ResponseEntity<Map> list(@RequestParam(required = false, name = "name") String name,
                                    @RequestParam(required = false, name = "location") String location,
                                    @RequestParam(required = false, name = "open") Boolean open,
                                    @PageableDefault(page = 0, size = 10) Pageable pageable) {


        Specification<Merchant> spec = ((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (name != null && !name.isEmpty()) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + name.toLowerCase() + "%"));
            }
            if (location != null && !location.isEmpty()) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("location")), "%" + location.toLowerCase() + "%"));
            }
            if (open != null) {
                predicates.add(criteriaBuilder.equal(root.get("open"), open));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        });

        Page<Merchant> clientCompanyList = merchantRepository.findAll(spec, pageable);
        return new ResponseEntity<Map>(response.sukses(clientCompanyList), HttpStatus.OK);
    }

    @Autowired
    private PdfService pdfService;

    @GetMapping("/generate")
    public ResponseEntity<InputStreamResource> generatePdf() {
        ByteArrayInputStream bis = pdfService.generatePdf();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=merchants.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

}

