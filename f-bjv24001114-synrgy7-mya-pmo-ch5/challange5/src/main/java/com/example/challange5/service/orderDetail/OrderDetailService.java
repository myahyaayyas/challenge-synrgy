package com.example.challange5.service.orderDetail;

import com.example.challange5.entity.OrderDetail;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface OrderDetailService {

    Map createOrderDetail(OrderDetail orderDetail);

    Map findAll(Pageable pageable);

    Map delete(UUID id);

    Map findById(UUID id);

    Map update(UUID id, UUID productId, Integer quantity);
}
