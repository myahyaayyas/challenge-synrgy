package com.example.challange5.service.user;

import com.example.challange5.entity.User;
import org.springframework.data.domain.Pageable;

import java.util.Map;
import java.util.UUID;

public interface UserService {

    Map save(User user);

    Map deleteUsers(UUID id);

    Map findById(UUID id);

    Map findAll(Pageable pageable);

    Map update(UUID uuid, User user);
}
