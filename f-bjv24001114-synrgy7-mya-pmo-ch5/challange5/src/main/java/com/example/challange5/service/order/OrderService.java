package com.example.challange5.service.order;

import com.example.challange5.entity.Order;
import com.example.challange5.entity.Product;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface OrderService {

    Map save(Order order);

    Map delete(UUID id);

    Map findById(UUID id);

    Map findAll(Pageable pageable);

    Map update(UUID uuid, Order order);

    List findOrderByUsersId(UUID usersId);
}
