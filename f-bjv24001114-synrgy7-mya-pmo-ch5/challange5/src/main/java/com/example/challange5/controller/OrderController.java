package com.example.challange5.controller;

import com.example.challange5.entity.Order;
import com.example.challange5.entity.Product;
import com.example.challange5.repository.OrderRepository;
import com.example.challange5.service.order.OrderService;
import com.example.challange5.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private Response response;

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<?> create(@RequestBody Order order) {
        return ResponseEntity.ok(orderService.save(order));
    }

    @DeleteMapping(value = {"/delete/{id}", "/delete/{id}/"})
    public ResponseEntity<?> delete(@PathVariable UUID id) {
        Map response = orderService.delete(id);
        return ResponseEntity.ok(response);
    }

    @PutMapping(value = {"/update/{id}", "/update/{id}/"})
    public ResponseEntity<?> edit(@PathVariable UUID id, @RequestBody Order order) {

        return ResponseEntity.ok(orderService.update(id, order));
    }

    @GetMapping()
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "0") Integer page,
                                     @RequestParam(defaultValue = "5") Integer size) {
        return ResponseEntity.ok(orderService.findAll(PageRequest.of(page, size)));
    }

    @GetMapping(value = {"/{id}", "/{id}"})
    public ResponseEntity<?> findById(@PathVariable UUID id) {
        return ResponseEntity.ok(orderService.findById(id));
    }

    @GetMapping(value = {"/users/{usersId}", "/users/{usersId}/"})
    public List findOrderByUsersId(@PathVariable UUID usersId) {
        return orderService.findOrderByUsersId(usersId);
    }

//    @GetMapping(value = { "/list-oder", "/list-oder/" })
//    public ResponseEntity<Map> list(@RequestParam(required = false, name = "username") String username,
//                                    @RequestParam(required = false, name = "emailAddress") String emailAddress,
//                                    @RequestParam(required = false, name = "password") String password,
//                                    @PageableDefault(page = 0, size = 10) Pageable pageable) {
//
//
//        Specification<User> spec = ((root, query, criteriaBuilder) -> {
//            List<Predicate> predicates = new ArrayList<>();
//            if (username != null && !username.isEmpty()) {
//                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("username")), "%" + username.toLowerCase() + "%"));
//            }
//            if (emailAddress != null && !emailAddress.isEmpty()) {
//                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("emailAddress")), "%" + emailAddress.toLowerCase() + "%"));
//            }
//            if (password != null && !password.isEmpty()) {
//                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("password")), "%" + password.toLowerCase() + "%"));
//            }
//
//            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
//        });
//
//        Page<User> clientCompanyList = userRepository.findAll(spec, pageable);
//        return new ResponseEntity<Map>(response.sukses(clientCompanyList), HttpStatus.OK);
//    }

}
