package com.example.challange5.repository;

import com.example.challange5.entity.Merchant;
import com.example.challange5.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;


@Repository
public interface ProductRepository extends JpaRepository<Product, UUID>, JpaSpecificationExecutor<Product> {
    List<Product> findByMerchantId(UUID merchantId);

    List<Product> findByStock(Boolean stock);
}

